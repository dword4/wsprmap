# wsprmap

A tool to generate interactive Plotly maps based on WSPR data

![example map output](https://i.imgur.com/bOPhILn.png)

## Requirements
- Python 3 (because reasons)
- Pandas 0.21.0
- Arrow
- Plotly

There is an issue with more recent versions of Pandas which prevents it from working

## Notes

There is _no_ filtering built into this code, you need to come up with the input 
file yourself using a tool such as grep to isolate pertinent records in the wsrpnet.org
daily log dumps

- start and end variables

These must be updated to contain the range of dates for the log file or nothing will work right

- Pandas quirks

Later versions of Pandas have a problem with the display.height option being set in the manner I do it. Be
sure to check these values to make sure that you arent feeding a log file larger than these values or you will
miss data in the maps